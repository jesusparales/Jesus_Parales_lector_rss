
require 'rest-client'
require 'json'
require 'date'


 class Reddit
  attr_accessor :articles, :file

  def initialize(url)
    @articles = RestClient.get(url)
    @file = JSON.parse(@articles.body)
  end

  def news
        @file['data']['children'].each do |key|
          fecha = Time.at(key['data']['created'].to_i).strftime("%d/%m/%Y %k:%M")  
            puts "Titulo ".italic + "#{key ['data']['title']}".colorize(:cyan)
            puts "Autor ".italic + "#{key['data']['author']}".colorize(:cyan)
            puts "Fecha: ".italic + "#{fecha}".colorize(:cyan)
            puts "Link ".italic + "#{key['data']['url']}".colorize(:cyan)
            puts
        end
      end
    end

impresion = Reddit.new('https://www.reddit.com/.json')

